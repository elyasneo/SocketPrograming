package student;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import main.Server;
import model.Student;
import model.Teacher;
import javafx.application.Application;
import javafx.stage.Stage;
import student.StudentAppController;
import utility.AlertBox;
import utility.Utility;

import java.io.*;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;

public class StudentApp extends Application {
    public static Student student;
    public static Socket socketControl;
    public static Socket socketChat;
    public static int port;
    public static DataInputStream dis;
    public static DataOutputStream dos;


    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("student.fxml"));
        String name = AlertBox.display("enter your name", false);
        dos.writeUTF(name);
        System.out.println(dis.readUTF());
        Student mStudent = Utility.stringToStudent(dis.readUTF());
        student = mStudent;
        System.out.println(mStudent.name);
        String teachersStr = dis.readUTF();
        List<Teacher> teachers = Utility.stringListTeacher(teachersStr);
        Teacher selectedTeacher = AlertBox.selectTeacher(teachers);
        port = selectedTeacher.portMultiCast;

        StudentAppController studentAppController = new StudentAppController(socketChat, socketControl);
        fxmlLoader.setController(studentAppController);
        Parent root = fxmlLoader.load();

        primaryStage.setTitle(student.name + " (teacher: " + selectedTeacher.name + ")");
        primaryStage.setScene(new Scene(root, 640, 480));
        primaryStage.show();
        primaryStage.setOnCloseRequest(e -> {
            try {
                socketControl.close();
                socketChat.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            System.exit(0);
        });

    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            socketControl = new Socket(Server.SERVER_URL, Server.PORT_CONTROL);
            socketChat = new Socket(Server.SERVER_URL, Server.PORT_CHAT);
            dis = new DataInputStream(socketControl.getInputStream());
            dos = new DataOutputStream(socketControl.getOutputStream());
            dos.writeUTF("student");
        } catch (Exception e) {
            e.printStackTrace();
        }
        launch(args);
    }
}
