package student;

import main.Server;
import teacher.TeacherApp;
import utility.Utility;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import java.util.ResourceBundle;

public class StudentAppController implements Initializable {

    private final DataOutputStream dos;
    private final DataOutputStream dosChat;
    private final DataInputStream dis;
    private final DataInputStream disChat;
    @FXML
    public ImageView imageView;
    @FXML
    public TextField chatTextField;
    @FXML
    public Button btnSend;

    @FXML
    public Button showStream;

    @FXML
    public TextArea chatTextArea;
    private boolean isStreaming = false;

    public StudentAppController(Socket socketChat, Socket socket) throws IOException {
        this.dos = new DataOutputStream(socket.getOutputStream());
        this.dosChat = new DataOutputStream(socketChat.getOutputStream());
        this.dis = new DataInputStream(socket.getInputStream());
        this.disChat = new DataInputStream(socketChat.getInputStream());
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnSend.setOnAction(e -> {
            try {
                handelChatMessage();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        chatTextField.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                try {
                    handelChatMessage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        new Thread(() -> {
            while (true) {
                try {
                    String s = disChat.readUTF();
                    chatTextArea.setText(chatTextArea.getText() + "\n" + s);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        showStream.setOnAction(e -> {
            showStream.setText("end stream");
            if (isStreaming) {
                isStreaming = false;
                showStream.setText("show stream");
            } else {
                new Thread(() -> {
                    isStreaming = true;
                    try (MulticastSocket multicastSocket = new MulticastSocket(StudentApp.port)) {
                        InetAddress inetAddress = InetAddress.getByName(Server.SERVER_MULTI_CAST_URL);
                        multicastSocket.joinGroup(inetAddress);
                        while (isStreaming) {
                            DatagramPacket datagramPacket = new DatagramPacket(new byte[100240], 100240, inetAddress, StudentApp.port);
                            multicastSocket.receive(datagramPacket);
                            Image image = Utility.getImageFromBytesArray(datagramPacket.getData());
                            imageView.setImage(image);

                        }
                        imageView.setImage(null);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }).start();
            }
        });


    }

    private void handelChatMessage() throws IOException {
        String msg = chatTextField.getText();
        if (msg != null)
            dosChat.writeUTF(msg);
        chatTextField.setText(null);
    }

}
