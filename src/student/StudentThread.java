package student;

import main.Server;
import model.Student;
import model.Teacher;
import utility.Utility;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;
import java.util.Arrays;


public class StudentThread extends Thread {
    private int id;
    private Socket connectionControl;
    private Socket connectionChat;

    public StudentThread(Socket connectionControl, Socket connectionChat) {
        this.connectionControl = connectionControl;
        this.connectionChat = connectionChat;
    }

    @Override
    public void run() {
        try {
            DataOutputStream dos = new DataOutputStream(connectionControl.getOutputStream());
            DataInputStream dis = new DataInputStream(connectionControl.getInputStream());
            String name = dis.readUTF();
            Student student = new Student(Server.indexStudent, name, connectionChat);
            id = Server.indexStudent;
            Server.students[Server.indexStudent++] = student;
            dos.writeUTF("your id is " + student.id);
            dos.writeUTF(Utility.studentToString(student));
            dos.writeUTF(Utility.listTeacherToString(Arrays.asList(Server.teachers).subList(0, Server.indexTeacher)));
            new Thread(() -> {
                DataInputStream disChat;
                DataOutputStream dosChat;
                try {
                    dosChat = new DataOutputStream(connectionChat.getOutputStream());
                    disChat = new DataInputStream(connectionChat.getInputStream());
                    while (true) {
                        String s = disChat.readUTF();
                        String msg;
                        String[] temp = null;
                        if (s.charAt(0) == '@') {
                            temp = s.split(" ", 2);
                            msg = " (Private Msg) " + temp[1];
                            temp[0] = temp[0].substring(1);
                        } else {
                            msg = s;
                        }
                        DataOutputStream outputStream;
                        for (int i = 0; i < Server.indexTeacher; i++) {
                            if (temp != null) {
                                if (temp[0].equals(Server.teachers[i].name)) {
                                    if (!Server.teachers[i].socket.isClosed()) {
                                        outputStream = new DataOutputStream(Server.teachers[i].socket.getOutputStream());
                                        outputStream.writeUTF(Server.students[id].name + " : " + msg);
                                    }
                                    if (!Server.students[id].socket.isClosed())
                                        dosChat.writeUTF(Server.students[id].name + " : " + msg);
                                }

                            } else {
                                if (!Server.teachers[i].socket.isClosed()) {
                                    outputStream = new DataOutputStream(Server.teachers[i].socket.getOutputStream());
                                    outputStream.writeUTF(Server.students[id].name + " : " + msg);
                                }
                            }

                        }
                        for (int i = 0; i < Server.indexStudent; i++) {
                            if (temp != null) {
                                if (temp[0].equals(Server.students[i].name)) {
                                    if (!Server.students[i].socket.isClosed()) {
                                        outputStream = new DataOutputStream(Server.students[i].socket.getOutputStream());
                                        outputStream.writeUTF(Server.students[id].name + " : " + msg);
                                    }
                                    if (!Server.students[id].socket.isClosed())
                                        dosChat.writeUTF(Server.students[id].name + " : " + msg);
                                }

                            } else {
                                if (!Server.students[i].socket.isClosed()) {
                                    outputStream = new DataOutputStream(Server.students[i].socket.getOutputStream());
                                    outputStream.writeUTF(Server.students[id].name + " : " + msg);
                                }

                            }

                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    try {
                        connectionControl.close();
                        connectionChat.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }


            }).start();

        } catch (Exception e) {
            e.printStackTrace();
            try {
                connectionControl.close();
                connectionChat.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }

}
