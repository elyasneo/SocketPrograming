package model;

import java.io.Serializable;
import java.net.Socket;

public class Teacher implements Serializable {
    public int id;
    public int portMultiCast;
    public String name;
    public String field;
    public Socket socket;

    public Teacher(int id, String name, int portMultiCast, Socket socket, String field) {
        this.id = id;
        this.portMultiCast = portMultiCast;
        this.name = name;
        this.socket = socket;
        this.field = field;
    }
}
