package model;

import java.io.Serializable;
import java.net.Socket;

public class Student implements Serializable {
    public int id;
    public String name;
    public Socket socket;

    public Student(int id, String name, Socket socket) {
        this.id = id;
        this.name = name;
        this.socket = socket;
    }
}
