package main;

import model.Student;
import model.Teacher;
import student.StudentThread;
import teacher.TeacherThread;

import java.io.*;
import java.net.*;

public class Server {
    public static Teacher[] teachers;
    public static Student[] students;
    public static int indexTeacher = 0;
    public static int indexStudent = 0;
    public static final String SERVER_URL = "localhost";
    public static final String SERVER_MULTI_CAST_URL = "224.2.2.2";
    public static final int PORT_CONTROL = 1221;
    public static final int PORT_CHAT = 1231;

    public static void main(String[] args) throws IOException {
        teachers = new Teacher[10];
        students = new Student[10];
        InetAddress inetAddress = InetAddress.getByName(SERVER_URL);
        ServerSocket controlSocket = new ServerSocket(PORT_CONTROL);
        ServerSocket chatSocket = new ServerSocket(PORT_CHAT);

        while (true) {
            try {
                Socket connectionControl = controlSocket.accept();
                Socket connectionChat = chatSocket.accept();
                DataOutputStream dos = new DataOutputStream(connectionControl.getOutputStream());
                DataInputStream dis = new DataInputStream(connectionControl.getInputStream());
                String role = dis.readUTF();
                if (role.equals("teacher")) {
                    new TeacherThread(connectionControl, connectionChat).start();
                } else if (role.equals("student")) {
                    new StudentThread(connectionControl, connectionChat).start();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }
}
