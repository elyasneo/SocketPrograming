package teacher;

import main.Server;
import model.Teacher;
import student.StudentApp;
import utility.Utility;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;


public class TeacherThread extends Thread {
    private int id;
    private Socket connectionControl;
    private Socket connectionChat;

    public TeacherThread(Socket connectionControl, Socket connectionChat) {
        this.connectionControl = connectionControl;
        this.connectionChat = connectionChat;
    }

    @Override
    public void run() {
        try {
            DataOutputStream dos = new DataOutputStream(connectionControl.getOutputStream());
            DataInputStream dis = new DataInputStream(connectionControl.getInputStream());
            String[] nameField = dis.readUTF().split("--");
            ServerSocket serverSocketTemp = new ServerSocket(0);
            int port = serverSocketTemp.getLocalPort();
            serverSocketTemp.close();
            Teacher teacher = new Teacher(Server.indexTeacher, nameField[0], port, connectionChat, nameField[1]);
            id = Server.indexTeacher;
            Server.teachers[Server.indexTeacher++] = teacher;
            dos.writeUTF("your id is " + teacher.id + " and your stream port is " + teacher.portMultiCast);
            dos.writeUTF(Utility.teacherToString(teacher));
            new Thread(() -> {
                DataInputStream disChat;
                DataOutputStream dosChat;
                try {
                    dosChat = new DataOutputStream(connectionChat.getOutputStream());
                    disChat = new DataInputStream(connectionChat.getInputStream());
                    while (true) {
                        String s = disChat.readUTF();
                        String msg;
                        String[] temp = null;
                        if (s.charAt(0) == '@') {
                            temp = s.split(" ", 2);
                            msg = " (Private Msg) " + temp[1];
                            temp[0] = temp[0].substring(1);
                        } else {
                            msg = s;
                        }
                        DataOutputStream outputStream;
                        for (int i = 0; i < Server.indexTeacher; i++) {
                            if (temp != null) {
                                if (temp[0].equals(Server.teachers[i].name)) {
                                    if (!Server.teachers[i].socket.isClosed()) {
                                        outputStream = new DataOutputStream(Server.teachers[i].socket.getOutputStream());
                                        outputStream.writeUTF(Server.teachers[id].name + " : " + msg);
                                    }
                                    dosChat.writeUTF(Server.teachers[id].name + " : " + msg);
                                }

                            } else {

                                if (!Server.teachers[i].socket.isClosed()) {
                                    outputStream = new DataOutputStream(Server.teachers[i].socket.getOutputStream());
                                    outputStream.writeUTF(Server.teachers[id].name + " : " + msg);
                                }
                            }

                        }
                        for (int i = 0; i < Server.indexStudent; i++) {
                            if (temp != null) {
                                if (temp[0].equals(Server.students[i].name)) {
                                    if (!Server.students[i].socket.isClosed()) {
                                        outputStream = new DataOutputStream(Server.students[i].socket.getOutputStream());
                                        outputStream.writeUTF(Server.teachers[id].name + " : " + msg);
                                    }
                                    dosChat.writeUTF(Server.teachers[id].name + " : " + msg);
                                }

                            } else {
                                if (!Server.students[i].socket.isClosed()) {
                                    outputStream = new DataOutputStream(Server.students[i].socket.getOutputStream());
                                    outputStream.writeUTF(Server.teachers[id].name + " : " + msg);
                                }

                            }

                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    try {
                        connectionControl.close();
                        connectionChat.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }


            }).start();


        } catch (Exception e) {
            e.printStackTrace();
            try {
                connectionControl.close();
                connectionChat.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }

    }

}
