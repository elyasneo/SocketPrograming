package teacher;

import main.Server;
import model.Teacher;
import utility.Utility;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.*;
import java.util.ResourceBundle;

public class TeacherAppController implements Initializable {

    private Socket socketChat;
    private final DataOutputStream dos;
    private final DataOutputStream dosChat;
    private final DataInputStream dis;
    private final DataInputStream disChat;
    private Socket socketControl;
    @FXML
    public ImageView imageView;
    @FXML
    public TextField chatTextField;
    @FXML
    public Button btnSend;
    @FXML
    public Button startStream;
    @FXML
    public TextArea chatTextArea;

    public boolean isStreaming = false;

    public TeacherAppController(Socket socketChat, Socket socketControl) throws IOException {
        this.socketChat = socketChat;
        this.dos = new DataOutputStream(socketControl.getOutputStream());
        this.dosChat = new DataOutputStream(socketChat.getOutputStream());
        this.dis = new DataInputStream(socketControl.getInputStream());
        this.disChat = new DataInputStream(socketChat.getInputStream());
        this.socketControl = socketControl;
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnSend.setOnAction(e -> {
            try {
                handelChatMessage();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        chatTextField.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                try {
                    handelChatMessage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        startStream.setOnAction(e -> {
            startStream.setText("end stream");
            if (isStreaming) {
                isStreaming = false;
                startStream.setText("start stream");
            } else {
                new Thread(() -> {
                    isStreaming = true;
                    try {
                        MulticastSocket multicastSocket = new MulticastSocket(TeacherApp.teacher.portMultiCast);
                        InetAddress address = InetAddress.getByName(Server.SERVER_MULTI_CAST_URL);
                        multicastSocket.joinGroup(address);
                        int i = 1;
                        while (isStreaming) {
                            if (i > 127) i = 1;
                            String path;
                            if (TeacherApp.teacher.field.toLowerCase().equals("zoo"))
                                path = "img/Zoology/";
                            else
                                path = "img/Astronomy/";
                            File file = new File(path + "frame (" + i + ").jpeg");
                            byte[] bytes = Utility.getBytesOfFile(file);
                            DatagramPacket datagramPacket = new DatagramPacket(bytes, bytes.length, address, TeacherApp.teacher.portMultiCast);
                            multicastSocket.send(datagramPacket);
                            imageView.setImage(Utility.getImageFromBytesArray(bytes));
                            System.out.println("send packet #" + i);
                            i++;
                            Thread.sleep(100);

                        }
                        DatagramPacket datagramPacket = new DatagramPacket(new byte[0], 0, address, TeacherApp.teacher.portMultiCast);
                        multicastSocket.send(datagramPacket);
                        imageView.setImage(null);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }).start();
            }

        });

        new Thread(() -> {
            while (true) {
                try {
                    String s = disChat.readUTF();
                    chatTextArea.setText(chatTextArea.getText() + "\n" + s);
                } catch (IOException e) {
                    e.printStackTrace();
                    try {
                        socketChat.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }).start();


    }

    private void handelChatMessage() throws IOException {
        String msg = chatTextField.getText();
        if (msg != null)
            dosChat.writeUTF(msg);
        chatTextField.setText(null);
    }

}
