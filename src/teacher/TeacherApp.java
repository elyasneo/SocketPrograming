package teacher;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import main.Server;
import model.Teacher;
import javafx.application.Application;
import javafx.stage.Stage;
import student.StudentAppController;
import utility.AlertBox;
import utility.Utility;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;

public class TeacherApp extends Application {
    public static Teacher teacher;
    private static Socket socketControl;
    private static Socket socketChat;
    private static DataInputStream dis;
    private static DataOutputStream dos;


    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("teacher.fxml"));
        TeacherAppController teacherAppController = new TeacherAppController(socketChat, socketControl);
        fxmlLoader.setController(teacherAppController);
        Parent root = fxmlLoader.load();
        String nameField = AlertBox.display("Enter your name and field", true);
        dos.writeUTF(nameField);
        System.out.println(dis.readUTF());
        teacher = Utility.stringToTeacher(dis.readUTF());
        System.out.println(teacher.name);
        primaryStage.setTitle(teacher.name);
        primaryStage.setScene(new Scene(root, 640, 480));
        primaryStage.show();
        primaryStage.setOnCloseRequest(e -> {
            System.exit(0);
        });

    }


    public static void main(String[] args) {
        try {
            socketControl = new Socket(Server.SERVER_URL, Server.PORT_CONTROL);
            socketChat = new Socket(Server.SERVER_URL, Server.PORT_CHAT);
            dis = new DataInputStream(socketControl.getInputStream());
            dos = new DataOutputStream(socketControl.getOutputStream());
            dos.writeUTF("teacher");
        } catch (Exception e) {
            e.printStackTrace();
        }
        launch(args);
    }
}
