package utility;

import model.Student;
import model.Teacher;
import javafx.scene.image.Image;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Utility {
    public static byte[] getBytesOfFile(File file) {
        FileInputStream fis;
        try {
            fis = new FileInputStream(file);
            //System.out.println(file.exists() + "!!");
            //InputStream in = resource.openStream();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            try {
                for (int readNum; (readNum = fis.read(buf)) != -1; ) {
                    bos.write(buf, 0, readNum); //no doubt here is 0
                    //Writes len bytes from the specified byte array starting at offset off to this byte array output stream.
                }
            } catch (IOException ex) {
                Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
            }
            byte[] bytes = bos.toByteArray();

            return bytes;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    public static Image getImageFromBytesArray(byte[] bytes) {
        return new Image(new ByteArrayInputStream(bytes));
    }

    public static String teacherToString(Teacher teacher) {
        return teacher.name + "--" + teacher.id + "--" + teacher.portMultiCast + "--" + teacher.field;
    }

    public static String studentToString(Student student) {
        return student.name + "--" + student.id;
    }

    public static Teacher stringToTeacher(String s) {
        String[] data = s.split("--");
        return new Teacher(Integer.parseInt(data[1]), data[0], Integer.parseInt(data[2]), null, data[3]);
    }

    public static Student stringToStudent(String s) {
        String[] data = s.split("--");
        return new Student(Integer.parseInt(data[1]), data[0], null);
    }

    public static String listTeacherToString(List<Teacher> teachers) {
        try {
            StringBuilder s = new StringBuilder();
            for (Teacher teacher : teachers) {
                s.append(teacherToString(teacher)).append("__");
            }
            int index = s.lastIndexOf("__");
            if (!s.toString().equals(""))
                s.delete(index, s.length());
            return s.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

    public static List<Teacher> stringListTeacher(String s) {
        if (s.equals(""))
            return new ArrayList<>();
        List<Teacher> teachers = new ArrayList<>();
        String[] data = s.split("__");
        for (String t : data) {
            teachers.add(stringToTeacher(t));
        }
        return teachers;
    }

}
