package utility;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Teacher;

import java.util.List;

public class AlertBox {
    private static Teacher mTeacher;

    public static String display(String title, boolean isTeacher) {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setMinWidth(400);
        TextField name = new TextField();
        name.setPromptText("name");
        TextField field = new TextField();
        field.setPromptText("field");
        Button button = new Button();
        button.setText("OK");
        button.setOnAction(e -> {
            window.close();
        });

        VBox layout = new VBox(10);
        if (isTeacher)
            layout.getChildren().addAll(name, field, button);
        else
            layout.getChildren().addAll(name, button);
        layout.setAlignment(Pos.CENTER);
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();
        if (isTeacher)
            return name.getText() + "--" + field.getText();
        else
            return name.getText();


    }

    public static Teacher selectTeacher(List<Teacher> teachers) {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("select");
        window.setMinWidth(400);
        VBox layout = new VBox(10);
        for (Teacher teacher : teachers) {
            Button button = new Button();
            button.setText(teacher.name + " (field: " + teacher.field + ")");
            button.setOnAction(e -> {
                mTeacher = teacher;
                window.close();
            });
            layout.getChildren().add(button);
        }

        layout.setAlignment(Pos.CENTER);
        Scene scene = new Scene(layout);
        window.setScene(scene);
        window.showAndWait();
        return mTeacher;


    }
}
